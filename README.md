# Desafio SambaTech

Esse projeto consiste na criação de um API que possa criar, listar/buscar, remover e editar uma mídia, também nesse projeto foi feito a integração com a AWS S3 para armazenamento do arquivo bruto.

## Como iniciar o projeto 

Segue as instruções para baixar e rodar a aplicação localmente

### Pré-requisitos

É necessário ter instlado na máquina JDK 8, Maven e uma IDE de sua preferencia

### Instalação

Primeiramente faça o clone do projeto

Com o clone feito importe esse projeto como um projeto maven em sua IDE 

Após o projeto ser importado espere o build ser feito e em seguida abra e execute o arquivo MediaStorageApplication.java com isso o servidor será iniciado
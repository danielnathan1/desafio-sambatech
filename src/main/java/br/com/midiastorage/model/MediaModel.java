package br.com.midiastorage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "midia")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MediaModel implements Serializable {

	private static final long serialVersionUID = -8806423198926681029L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	@Column(nullable = false, length = 512)
	private String nome;
	
	@Column(length = 512)
	private String url;
	
	@Column
	private Integer duracao;
	
	@Column(nullable = false)
	private Date upload;
	
	@Column(nullable = false)
	private boolean	deleted;
	
	@Column(nullable = false)
	private String awsReference;
	
	@Column(nullable = false)
	private String keyName;

}

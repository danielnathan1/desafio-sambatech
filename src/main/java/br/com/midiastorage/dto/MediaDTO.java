package br.com.midiastorage.dto;

import java.sql.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaDTO {
	
	private String nome;

	private String url;

	private Integer duracao;

	private Date upload;

	private Boolean deleted;
}

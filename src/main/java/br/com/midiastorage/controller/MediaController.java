package br.com.midiastorage.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.midiastorage.dto.MediaDTO;
import br.com.midiastorage.model.MediaModel;
import br.com.midiastorage.service.MediaService;

@RestController
@RequestMapping("/medias")
public class MediaController {

	@Autowired
	private MediaService mediaService;

	@Autowired
	private ModelMapper modelMapper;

	@PostMapping
	public MediaModel saveMedia(@RequestBody MediaDTO mediaDTO) {
		return mediaService.saveMedia(modelMapper.map(mediaDTO, MediaModel.class));
	}

	@GetMapping
	public List<MediaModel> listMedias(@RequestParam("notDeleted") boolean notDeleted) {
		return mediaService.listMedias(notDeleted);
	}

	@GetMapping("/{id}")
	public MediaModel findMediaById(@PathVariable(value = "id") Long id) {
		return mediaService.findMediaById(id);
	}

	@PutMapping(path = "/{id}")
	public MediaModel updateMedia(@PathVariable(value = "id") Long id, @RequestBody MediaDTO mediaDTO) {
		return mediaService.updateMedia(id, modelMapper.map(mediaDTO, MediaModel.class));
	}

	@DeleteMapping("/{id}")
	public void deleteMedia(@PathVariable(value = "id") Long id) {
		mediaService.deleteMedia(id);
	}

}

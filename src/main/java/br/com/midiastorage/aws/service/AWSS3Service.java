package br.com.midiastorage.aws.service;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

@Service
public class AWSS3Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3Service.class);

	@Autowired
	private AmazonS3 amazonS3;
	@Value("${aws.s3.bucket}")
	private String bucketName;

	@Async
	public PutObjectResult uploadFile(String uniqueFileName, File file) {
		try {
			return uploadFileToS3Bucket(bucketName, file, uniqueFileName);
		} catch (final AmazonServiceException ex) {
			LOGGER.info("File upload is failed.");
			LOGGER.error("Error= {} while uploading file.", ex.getMessage());
		}
		return null;
	}
	
	public void deleteFile(final String keyName) {
		final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, keyName);
		amazonS3.deleteObject(deleteObjectRequest);
		LOGGER.info("File deleted successfully.");
	}
	
	private PutObjectResult uploadFileToS3Bucket(final String bucketName, final File file, String uniqueFileName) {
		final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
		return amazonS3.putObject(putObjectRequest);
	}
	
}
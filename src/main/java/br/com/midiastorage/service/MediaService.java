package br.com.midiastorage.service;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.model.PutObjectResult;

import br.com.midiastorage.aws.service.AWSS3Service;
import br.com.midiastorage.model.MediaModel;
import br.com.midiastorage.repository.MediaRepository;

@Service
public class MediaService {

	@Autowired
	public MediaRepository mediaRepository;

	@Autowired
	public AWSS3Service awsService;

	public MediaModel saveMedia(MediaModel mediaModel) {
		File file = new File(mediaModel.getUrl());
		final String uniqueFileName = LocalDateTime.now() + "_" + file.getName();
		if(StringUtils.isNotBlank(file.getPath())) {
			PutObjectResult retornoAWS = awsService.uploadFile(uniqueFileName, file);
			if(Objects.nonNull(retornoAWS)) {
				mediaModel.setAwsReference(retornoAWS.getETag());
				mediaModel.setKeyName(uniqueFileName);
				return mediaRepository.save(mediaModel);
			}
		}
		return null;
	}

	public MediaModel updateMedia(Long id, MediaModel mediaModel) {
		if (mediaExists(id)) {
			mediaModel.setId(id);
			return mediaRepository.save(mediaModel);
		}
		return null;
	}

	public List<MediaModel> listMedias(boolean notDeleted) {
		return notDeleted ? mediaRepository.findAllByDeleted(false) : mediaRepository.findAll();
	}

	public MediaModel findMediaById(Long id) {
		Optional<MediaModel> media = mediaRepository.findById(id);
		return media.isPresent() ? media.get() : null;
	}

	public void deleteMedia(Long id) {
		MediaModel mediaModel = findMediaById(id);
		if(Objects.nonNull(mediaModel)) {
			awsService.deleteFile(mediaModel.getKeyName());
			mediaRepository.deleteById(id);
		}
	}

	private boolean mediaExists(Long id) {
		return mediaRepository.existsById(id);
	}
}

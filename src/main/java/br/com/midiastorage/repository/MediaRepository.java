package br.com.midiastorage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.midiastorage.model.MediaModel;

public interface MediaRepository extends JpaRepository<MediaModel, Long> {
	public List<MediaModel> findAllByDeleted(boolean deleted);
}

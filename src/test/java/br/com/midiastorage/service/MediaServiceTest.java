package br.com.midiastorage.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.midiastorage.model.MediaModel;

@SpringBootTest
class MediaServiceTest {

	@Autowired
	MediaService mediaService;

	// Id para não criar lixo no banco durante a execução dos testes
	Long id;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testSaveMedia() {
		MediaModel mediaModel = new MediaModel();
		mediaModel.setDeleted(false);
		mediaModel.setDuracao(2000);
		mediaModel.setNome("teste-nome");
		mediaModel.setUpload(new Date());
		mediaModel.setUrl("src/test/java/br/com/midiastorage/service/test-file.txt");
		mediaModel = mediaService.saveMedia(mediaModel);
		assertTrue(Objects.nonNull(mediaModel));
		id = mediaModel.getId();
	}

	@Test
	void testFindMediaByIdWithWorngId() {
		assertFalse(Objects.nonNull(mediaService.findMediaById(0L)));
	}

	@Test
	void testUpdateMedia() {
		MediaModel mediaModel = new MediaModel();
		mediaModel.setDeleted(false);
		mediaModel.setDuracao(2500);
		mediaModel.setNome("teste-nome-alteracao");
		mediaModel.setUpload(new Date());

		assertTrue(Objects.nonNull(mediaService.updateMedia(1L, mediaModel)));
	}

	@Test
	void testUpdateMediaWithWorngId() {
		MediaModel mediaModel = new MediaModel();
		mediaModel.setDeleted(false);
		mediaModel.setDuracao(2500);
		mediaModel.setNome("teste-nome-alteracao");
		mediaModel.setUpload(new Date());

		assertFalse(Objects.nonNull(mediaService.updateMedia(0L, mediaModel)));
	}

	@Test
	void testFindMediaById() {
		assertTrue(Objects.nonNull(mediaService.findMediaById(1L)));
	}

}
